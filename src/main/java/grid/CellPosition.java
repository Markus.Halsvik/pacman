package grid;

public class CellPosition {
    private final int row;
    private final int col;

    public CellPosition (int row, int col) {
        this.row = row;
        this.col = col;
    }

    public int getRow() {
        return row;
    }
    
    public int getCol() {
        return col;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof CellPosition)) {
            return false;
        }

        CellPosition b = (CellPosition) o;

        if (this.row == b.getRow() && this.col == b.getCol()) {
            return true;
        }
        return false;
    }

    // Prime factorization permits each cellPosition to have a unique hash
    public int hashCode() {
        int result = (int) (Math.pow(2, row) * Math.pow(3, col));
        return result;
    }
}
