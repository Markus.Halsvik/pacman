package pacman;

import javax.swing.JFrame;

import pacman.controller.Controller;
import pacman.model.PacManMaze;
import pacman.model.PacManModel;
import pacman.view.PacManView;


public class Main {
  public static final String WINDOW_TITLE = "PacMan";
  
  public static void main(String[] args) throws Exception {
    // int rows = 23;
    // int cols = 25;
    int rows = 23;
    int cols = 25;
    PacManMaze maze = new PacManMaze(rows, cols);                    // int int
    PacManModel model = new PacManModel(maze);                       // TetrisBoard TetrominoFactory
    PacManView view = new PacManView(model);                         // TetrisModel
    Controller controller = new Controller(model, view); // TetrisModel TetrisView

    // The JFrame is the "root" application window.
    // We here set som properties of the main window, 
    // and tell it to display our tetrisView
    JFrame frame = new JFrame(WINDOW_TITLE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    // Here we set which component to view in our window
    frame.setContentPane(view);
    
    // Call these methods to actually display the window
    frame.pack();
    frame.setVisible(true);
  }
}
