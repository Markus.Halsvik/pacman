package pacman.view;

import java.util.ArrayList;

import grid.GridCell;
import grid.GridDimension;
import pacman.model.GameState;
import pacman.model.units.Unit;
import pacman.view.tiles.*;

public interface ViewableModel {

    /* Returns a GridDimension object. */
    public GridDimension getDimension();

    /* returns an iterator of all the GridCells on the board. */
    public Iterable <GridCell <Tile>> getTilesOnBoard();

    public Unit getPacMan();

    public ArrayList <Unit> getGhosts();

    /* returns the GameState of a given game. */
    public GameState getGameState();

    /* returns the GameState of a given game. */
    public int getScore();
}
