package pacman.view.tiles;

public class Tile {
    private Sprite sprite;
    private int [] connections;
    private String score;

    public Tile(Sprite sprite, int [] connections) {
        this.sprite = sprite;
        this.connections = connections;
    }

    public Tile(Sprite sprite, String score) {
        this.sprite = sprite;
        this.score = score;
    }

    public Tile(Sprite sprite) {
        this.sprite = sprite;
    }

    public void setSprite(Sprite tile) {
        this.sprite = tile;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setConnections(int [] connections) {
        this.connections = connections;
    }

    public String getConnections() {
        String cons = "";
        for (int i : connections) {
            cons += i;
        }
        return cons;
    }

    public String getScore() {
        return score;
    }

    public boolean isWall() {
        if (sprite == Sprite.WALL) {
            return true;
        }
        return false;
    }
}
