package pacman.view;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import grid.CellPosition;
import grid.GridCell;
import grid.GridDimension;
import pacman.model.GameState;
import pacman.model.units.Unit;
import pacman.view.tiles.Sprite;
import pacman.view.tiles.Tile;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

public class PacManView extends JPanel {
    private ViewableModel model;
    private GameState state;
    private final int side = 50;
    private final int height;
    private final int width;
    
    public PacManView(ViewableModel model) {
        GridDimension gd = model.getDimension();
        int rows = gd.getRows();
        int cols = gd.getCols();
        height = side * rows;
        width = side * cols;
        this.model = model;
        this.state = GameState.ACTIVE_GAME;
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(width, height));
    }
    
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        if (state == GameState.GAME_OVER) {
            drawGameOver(g2);
            return;
        }
        drawGame(g2);
    }

    private void drawGame(Graphics2D g2) {
        Rectangle2D board = new Rectangle2D.Double(0, 0, width, height);
        GridDimension gd = model.getDimension();
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(board, gd, 0);
        Iterable <GridCell <Tile>> cells = model.getTilesOnBoard();
        g2.draw(board);
        drawTiles(g2, cells, converter);
        drawScore(g2, model.getScore(), converter);
        drawUnits(g2, converter);
    }

    private void drawTiles(Graphics2D g2, Iterable <GridCell <Tile>> cells, CellPositionToPixelConverter converter) {
        for (GridCell <Tile> cell : cells) {
            CellPosition pos = cell.getPos();
            Tile tile = cell.getValue();
            drawSprite(g2, pos, tile, null, converter);
        }
        
    }

    private void drawScore(Graphics2D g2, int score, CellPositionToPixelConverter converter) {
        GridDimension gd = model.getDimension();
        int rows = gd.getRows();
        int cols = gd.getCols();
        int midRow = (rows - 1) / 2;
        int midCol = (cols - 1) / 2;
        String string = "" + score;
        for (int i = string.length() - 1; i >= 0; i--) {
            String c = "" + string.charAt(i);
            CellPosition pos = new CellPosition(midRow + 2, midCol + 1 - (string.length() - (i + 1)));
            Tile tile = new Tile(Sprite.SCORE, c);
            drawSprite(g2, pos, tile, null, converter);
        }  
    }

    private void drawUnits(Graphics2D g2, CellPositionToPixelConverter converter) {
        Unit pacman = model.getPacMan();
        CellPosition pos = pacman.getPos();
        drawSprite(g2, pos, null, pacman, converter);
        for (Unit ghost : model.getGhosts()) {
            pos = ghost.getPos();
            drawSprite(g2, pos, null, ghost, converter);
        }
    }


    private void drawSprite(Graphics2D g2, CellPosition pos, Tile tile, Unit unit, CellPositionToPixelConverter converter) {
        SpriteImageLoader sprites = new SpriteImageLoader();
        Rectangle2D cellBox = converter.getBoundsForCell(pos);
        double x = cellBox.getMinX();
        double y = cellBox.getMinY();
        double scale = side / 16;
        AffineTransform transform = new AffineTransform();
        transform.scale(scale, scale);
        transform.translate(x/(scale + 0.1), y/(scale + 0.1));
        if (tile != null) {
            Image image = sprites.getTileImage(tile);
            g2.drawImage(image, transform, null);
        }
        if (unit != null) {
            Image image = sprites.getUnitImage(unit);
            g2.drawImage(image, transform, null);
        }
    }

    public void gameOver() {
        this.state = GameState.GAME_OVER;
    }

    private void drawGameOver(Graphics2D g2) {
        GridDimension gd = model.getDimension();
        int rows = gd.getRows();
        int cols = gd.getCols();
        int midRow = (rows - 1) / 2;
        int midCol = (cols - 1) / 2;
        AffineTransform transform = new AffineTransform();
        transform.translate(midCol * side, midRow * side);
        try {
            String basePath = (new File("")).getAbsolutePath();
            Image image = ImageIO.read(new File(basePath + "\\src\\main\\java\\pacman\\view\\visuals\\gameover.png"));
            int dx = image.getWidth(null);
            int dy = image.getHeight(null);
            transform.translate(-dx / 2, -dy / 2);
            g2.drawImage(image, transform, null);
        }
        catch (IOException | IllegalArgumentException e) {}
    }
}
