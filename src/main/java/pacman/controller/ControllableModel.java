package pacman.controller;

import pacman.model.GameState;
import pacman.model.Direction;

public interface ControllableModel {
    
    /* Moves the player */
    public boolean movePacMan(Direction dir);
    
    /* Returns the gamestate of the current game */
    public GameState getGameState();
    
    /* Returns the timer delay for the ticks in the game */
    public int getTimerDelay();
    
    /* Executes some command per tick, in this case it drops the tetromino one block */
    public void clockTick();

    public void ghostChase();

    public int getFruits();

    public int getScore();

    public void newLevel();

    public void releaseGhosts();
}
