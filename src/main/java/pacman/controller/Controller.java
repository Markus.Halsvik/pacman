package pacman.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.Timer;

import pacman.midi.PacManSong;
import pacman.model.Direction;
import pacman.model.GameState;
import pacman.view.PacManView;

public class Controller implements KeyListener {
    private ControllableModel controller;
    private PacManView view;
    private Timer timer;
    private PacManSong song;
    private int tickCount;
    
    public Controller(ControllableModel controller, PacManView view) {
        this.controller = controller;
        this.view = view;
        view.addKeyListener(this);
        this.timer = new Timer(controller.getTimerDelay(), this::clockTick);
        timer.start();
        this.tickCount = 0;
        song = new PacManSong();
        song.run();
    }

    public void keyTyped(KeyEvent key) {

    }

    public void keyReleased(KeyEvent key) {

    }

    /* Provides a set of keyboard keys methods that run once pressed */
    public void keyPressed(KeyEvent key) {
        if (controller.getGameState() == GameState.GAME_OVER) {
            return;
        }
        if (key.getKeyCode() == KeyEvent.VK_UP) {
            controller.movePacMan(Direction.UP);
        }
        else if (key.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.movePacMan(Direction.LEFT);
        }
        else if (key.getKeyCode() == KeyEvent.VK_DOWN) {
            controller.movePacMan(Direction.DOWN);
        }
        else if (key.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.movePacMan(Direction.RIGHT);
        }
        view.repaint();
    }

    /* 
     * Sets the initial and in-between event delay for the timer.
     * Could be used to increase or decrease the tick-rate to
     * decrease or increase difficulty.
     */
    public void setNewTimerDelay(int timerDelay) {
        timer.setDelay(timerDelay);
        timer.setInitialDelay(timerDelay);
    }

    /*
     * This implementation checks the GameState and terminates the timer
     * if the game is over. Else it calls to the clockTick() in the TetrisModel class
     * which drops the tetromino one block and updates the TetrisView variable.
     */
    public void clockTick(ActionEvent tick) {
        controller.clockTick();
        view.repaint();
        if (controller.getGameState() == GameState.GAME_OVER) {
            view.gameOver();
            view.repaint();
            timer.stop();
            return;
        }
        if (tickCount < 10) {
            tickCount++;
        }
        else {
            controller.releaseGhosts();
        }
        if (controller.getScore() == controller.getFruits()) {
            tickCount = 0;
            controller.newLevel();
        }
    }
}
