package pacman.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import grid.CellPosition;
import grid.GridCell;
import grid.GridDimension;
import pacman.controller.ControllableModel;
import pacman.model.units.Unit;
import pacman.model.units.UnitType;
import pacman.view.ViewableModel;
import pacman.view.tiles.Sprite;
import pacman.view.tiles.Tile;

public class PacManModel implements ViewableModel, ControllableModel {
    private PacManMaze maze;
    private Unit pacMan;
    private ArrayList <Unit> ghosts;
    private GameState gameState;
    private boolean ghostsReleased;
    private int score;

    public PacManModel(PacManMaze maze) {
        this.maze = maze;
        ghostsReleased = false;
        initializePacMan();
        initializeGhosts();
        gameState = GameState.ACTIVE_GAME;
        score = 0;
    }

    public GameState getGameState() {
        return gameState;
    }

    public GridDimension getDimension() {
        return maze;
    }

    public Unit getPacMan() {
        return pacMan;
    }

    public ArrayList <Unit> getGhosts() {
        return ghosts;
    }

    public int getFruits() {
        return maze.getFruits();
    }

    public int getScore() {
        return score;
    }

    public void newLevel() {
        maze.newMaze();
        ghostsReleased = false;
        initializePacMan();
        initializeGhosts();
    }

    private void initializePacMan() {
        ArrayList <CellPosition> positions = new ArrayList<>();
        for (GridCell <Tile> cell : getTilesOnBoard()) {
            CellPosition pos = cell.getPos();
            Tile tile = cell.getValue();
            Sprite sprite = tile.getSprite();
            if (sprite == Sprite.FRUIT) {
                positions.add(pos);
            }
        }
        Random rand = new Random();
        int index = rand.nextInt(positions.size());

        UnitType type = UnitType.PLAYER;
        CellPosition unitPos = positions.get(index);
        int health = 1;
        Sprite sprite = Sprite.PACMAN_RIGHT;
        this.pacMan = new Unit(type, unitPos, health, sprite);
        maze.setValue(unitPos, new Tile(Sprite.OPEN));
        maze.setFruits();
    }

    private void initializeGhosts() {
        ghosts = new ArrayList<>(4);
        int midRow = (maze.getRows() - 1) / 2;
        int midCol = (maze.getCols() - 1) / 2;
        UnitType type = UnitType.ENEMY;
        CellPosition blinkyPos = new CellPosition(midRow - 2, midCol);
        CellPosition pinkyPos  = new CellPosition(midRow, midCol);
        CellPosition inkyPos   = new CellPosition(midRow, midCol - 1);
        CellPosition clydePos  = new CellPosition(midRow, midCol + 1);
        int health = 1;
        Sprite blinkySprite = Sprite.BLINKY;
        Sprite pinkySprite  = Sprite.PINKY;
        Sprite inkySprite   = Sprite.INKY;
        Sprite clydeSprite  = Sprite.CLYDE;
        Unit blinky = new Unit(type, blinkyPos, health, blinkySprite);
        Unit pinky  = new Unit(type, pinkyPos,  health, pinkySprite);
        Unit inky   = new Unit(type, inkyPos,   health, inkySprite);
        Unit clyde  = new Unit(type, clydePos,  health, clydeSprite);
        ghosts.add(blinky);
        ghosts.add(pinky);
        ghosts.add(inky);
        ghosts.add(clyde);
    }

    public void updateMaze() {
        this.maze.newMaze();
    }

    public Iterable <GridCell <Tile>> getTilesOnBoard() {
        ArrayList <GridCell <Tile>> tiles = new ArrayList<>();
        Iterator <GridCell <Tile>> iterator = maze.iterator();
        while (iterator.hasNext()) {
            tiles.add(iterator.next());
        }
        return tiles;
    }

    public boolean movePacMan(Direction dir) {
        CellPosition pos = pacMan.getPos();
        Tile tile = maze.peekTile(pos, dir, 1);
        if (tile.isWall()) {
            return false;
        }
        if (dir == Direction.UP) {
            pacMan.setSprite(Sprite.PACMAN_UP);
        }
        else if (dir == Direction.LEFT) {
            pacMan.setSprite(Sprite.PACMAN_LEFT);
        }
        else if (dir == Direction.DOWN) {
            pacMan.setSprite(Sprite.PACMAN_DOWN);
        }
        else {
            pacMan.setSprite(Sprite.PACMAN_RIGHT);
        }

        CellPosition newPos = maze.peekPos(pos, dir, 1);
        pacMan.setPos(newPos);
        for (Unit ghost : ghosts) {
            CellPosition ghostPos = ghost.getPos();
            if (ghostPos.equals(newPos)) {
                gameState = GameState.GAME_OVER;
            }
        }
        eatFruit();
        return true;
    }

    private void eatFruit() {
        CellPosition pos = pacMan.getPos();
        Tile tile = maze.getValue(pos);
        if (tile.getSprite() == Sprite.FRUIT) {
            tile.setSprite(Sprite.OPEN);
            maze.setValue(pos, tile);
            score++;
        }
    }

    private boolean moveGhost(Unit ghost, Direction dir) {
        CellPosition pos = ghost.getPos();
        CellPosition newPos = maze.peekPos(pos, dir, 1);
        for (Unit otherGhost : ghosts) {
            if (otherGhost.equals(ghost)) {
                continue;
            }
            CellPosition ghostPos = otherGhost.getPos();
            if (newPos.equals(ghostPos)) {
                return false;
            }
        }

        ghost.setPos(newPos);
        CellPosition pacPos = pacMan.getPos();
        if (pacPos.equals(newPos)) {
            gameState = GameState.GAME_OVER;
        }
        return true;
    }

    public void ghostChase() {
        if (ghostsReleased) {
            for (Unit ghost : ghosts) {
                CellPosition ghostPos = ghost.getPos();
                Direction dir = closestDirection(ghostPos);
                moveGhost(ghost, dir);
            }
            return;
        }
        for (Unit ghost : ghosts) {
            if (ghost.getSprite() == Sprite.BLINKY) {
                CellPosition ghostPos = ghost.getPos();
                Direction dir = closestDirection(ghostPos);
                moveGhost(ghost, dir);
            }
        }
    }

    private Direction closestDirection(CellPosition ghostPos) {
        class BFSChase {
            private ArrayList <CellPosition> queue;
            private HashMap <CellPosition, Integer> visited;
            private HashMap <CellPosition, CellPosition> closestPos;

            public BFSChase() {
                queue = new ArrayList<>();
                visited = new HashMap<>(maze.getRows() * maze.getCols());
                closestPos = new HashMap<>(maze.getRows() * maze.getCols());
                initializeVariables();
            }

            private void initializeVariables() {
                for (GridCell <Tile> cell : getTilesOnBoard()) {
                    CellPosition pos = cell.getPos();
                    Tile tile = cell.getValue();
                    Sprite sprite = tile.getSprite();
                    visited.put(pos, 0);
                    if (sprite == Sprite.WALL) {
                        visited.put(pos, 1);
                    }
                }
            }

            private Direction getDirection(CellPosition currPos, CellPosition nextPos) {
                if (currPos.getRow() > nextPos.getRow()) {
                    return Direction.UP;
                }
                if (currPos.getCol() > nextPos.getCol()) {
                    return Direction.LEFT;
                }
                if (currPos.getRow() < nextPos.getRow()) {
                    return Direction.DOWN;
                }
                return Direction.RIGHT;
            }

            public Direction bfsOuter(CellPosition startPos, CellPosition targetPos) {
                CellPosition nextPos = bfsInner(targetPos, startPos);
                return getDirection(startPos, nextPos);
            }

            private CellPosition bfsInner(CellPosition startPos, CellPosition targetPos) {
                visited.put(startPos, 1);
                queue.add(startPos);
                while (!queue.isEmpty()) {
                    CellPosition currPos = queue.get(0);
                    queue.remove(0);
                    ArrayList <CellPosition> neighbours = maze.getNeighbours(currPos, 1);
                    for (CellPosition neighPos : neighbours) {
                        if (visited.get(neighPos) == 1) {
                            continue;
                        }
                        closestPos.put(neighPos, currPos);
                        queue.add(neighPos);
                        visited.put(neighPos, 1);
                    }
                }
                return closestPos.get(targetPos);
            }
        }

        BFSChase chaser = new BFSChase();
        CellPosition pacPos = pacMan.getPos();
        return chaser.bfsOuter(ghostPos, pacPos);
    }

    public void releaseGhosts() {
        ghostsReleased = true;
        maze.openCenter();
    }

    public int getTimerDelay() {
        return 500;
    }   

    public void clockTick() {
        ghostChase();
    }
}
