package pacman.model;

import grid.CellPosition;
import grid.Grid;
import grid.GridCell;
import pacman.view.tiles.Sprite;
import pacman.view.tiles.Tile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;;

public class PacManMaze extends Grid <Tile> {
    private ArrayList <GridCell <Integer>> visited;
    private ArrayList <Direction> directions;
    private int fruits;


    public PacManMaze(int rows, int cols) {
        super(rows, cols);
        initializeVariables();
        mazeGenerator();
    }

    private void initializeVariables() {
        visited = new ArrayList<>(rows * cols);
        directions = new ArrayList <Direction> ();
        directions.add(Direction.UP);
        directions.add(Direction.LEFT);
        directions.add(Direction.DOWN);
        directions.add(Direction.RIGHT);
        fruits = 0;
    }

    public void newMaze(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        visited = new ArrayList<>(rows * cols);
        mazeGenerator();
    }

    public void newMaze() {
        visited = new ArrayList<>(rows * cols);
        mazeGenerator();
    }

    public int getFruits() {
        return fruits;
    }

    private void mazeGenerator() {
        initializeTiles();
        initializeVisited();
        CellPosition startPos = new CellPosition(1, 1);
        addCenter();
        DFS(startPos);
        removeDeadEnds();
        finalizeMaze();
    }

    private void initializeTiles() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                setValue(pos, new Tile(Sprite.WALL));
                if (i % 2 == 1 && j % 2 == 1) {
                    setValue(pos, new Tile(Sprite.FRUIT));
                }
            }
        }
    }

    private void initializeVisited() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                GridCell <Integer> cell = new GridCell <Integer> (pos, 0);
                visited.add(cell);
                if (i == 0 || i == rows - 1) {
                    cell = new GridCell<Integer>(pos, 1);
                    visited.set(indexFromPos(pos), cell);
                }
                if (j == 0 || j == cols - 1) {
                    cell = new GridCell<Integer>(pos, 1);
                    visited.set(indexFromPos(pos), cell);
                }
            }
        }
    }

    private void set(CellPosition pos, Integer value) {
        int index = indexFromPos(pos);
        GridCell <Integer> cell = new GridCell <Integer> (pos, value);
        visited.set(index, cell);
    }

    private Integer get(CellPosition pos) {
        int index = indexFromPos(pos);
        Integer value = visited.get(index).getValue();
        return value;
    }

    private void addCenter() { // 5 rows and 7 collumns
        int topRow = ((rows - 1) / 2) - 2;
        int leftCol = ((cols - 1) / 2) - 3;
        int midRow = topRow + 2;
        int midCol = leftCol + 3;
        int botRow = ((rows - 1) / 2) + 2;
        int rightCol = ((cols - 1) / 2) + 3;
        for (int i = topRow; i <= botRow; i++) {
            for (int j = leftCol; j <= rightCol; j++) {
                CellPosition pos = new CellPosition(i, j);
                if (getValue(pos).getSprite() == Sprite.FRUIT) {
                }
                setValue(pos, new Tile(Sprite.OPEN));
                if (i == topRow || i == botRow) {
                    setValue(pos, new Tile(Sprite.OPEN));
                }
                else if (j == leftCol || j == rightCol) {
                    setValue(pos, new Tile(Sprite.OPEN));
                }
                else if (i == midRow && midCol - 1 <= j && j <= midCol + 1) {
                    setValue(pos, new Tile(Sprite.OPEN));
                    set(pos, 1);
                }
                else {
                    setValue(pos, new Tile(Sprite.WALL));
                    set(pos, 1);
                }
            }
        }
    }

    private void DFS(CellPosition pos) {
        set(pos, 1);
        ArrayList <CellPosition> neighbours = getNeighbours(pos, 2);
        Random rand = new Random();
        while (neighbours.size() != 0) {
            int index = rand.nextInt(neighbours.size());
            CellPosition neighPos = neighbours.get(index);
            neighbours.remove(index);
            if (get(neighPos) == 1) {
                continue;
            }
            makePath(pos, neighPos);
            DFS(neighPos);
        }
    }

    public ArrayList <CellPosition> getNeighbours(CellPosition pos, int dist) {
        ArrayList <CellPosition> neighbours = new ArrayList<>(4);
        for (Direction dir : directions) {
            CellPosition neighPos = peekPos(pos, dir, dist);
            if (!isLegalTile(neighPos)) {
                continue;
            }
            neighbours.add(neighPos);
        }
        return neighbours;
    }

    private void makePath(CellPosition pos1, CellPosition pos2) {
        int row1 = pos1.getRow();
        int col1 = pos1.getCol();
        int row2 = pos2.getRow();
        int col2 = pos2.getCol();
        CellPosition wallPos = new CellPosition((row1 + row2) / 2, (col1 + col2) / 2);
        Tile tile = getValue(wallPos);
        if (tile.isWall()) {
            setValue(wallPos, new Tile(Sprite.FRUIT));
        }
    }

    public CellPosition peekPos(CellPosition pos, Direction dir, int dist) {
        int row = pos.getRow();
        int col = pos.getCol();
        if (dir == Direction.UP)    {return new CellPosition(row - dist, col       );}
        if (dir == Direction.LEFT)  {return new CellPosition(row       , col - dist);}
        if (dir == Direction.DOWN)  {return new CellPosition(row + dist, col       );}
        if (dir == Direction.RIGHT) {return new CellPosition(row       , col + dist);}
        System.out.println("Undefined direction.");
        return null;
    }

    public Tile peekTile(CellPosition pos, Direction dir, int dist) {
        CellPosition newPos = peekPos(pos, dir, dist);
        Tile tile = getValue(newPos);
        return tile;
    }

    private boolean isLegalTile(CellPosition pos) {
        int row = pos.getRow();
        int col = pos.getCol();
        if (row < 0 || row > rows - 1) {
            return false;
        }
        if (col < 0 || col > cols - 1) {
            return false;
        }
        return true;
    }

    private void removeDeadEnds() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                Tile tile = getValue(pos);
                if (!tile.isWall() && !isInCenter(pos)) {
                    removeDeadEnd(pos);
                }
            }
        }
    }

    private boolean isInCenter(CellPosition pos) {
        int topRow = ((rows - 1) / 2) - 2;
        int leftCol = ((cols - 1) / 2) - 3;
        int botRow = ((rows - 1) / 2) + 2;
        int rightCol = ((cols - 1) / 2) + 3;
        int row = pos.getRow();
        int col = pos.getCol();
        if (topRow <= row && row <= botRow && leftCol <= col && col <= rightCol) {
            return true;
        }
        return false;
    }

    private boolean isBorder(CellPosition pos) {
        int row = pos.getRow();
        int col = pos.getCol();
        if (row == 0 || row == rows - 1) {
            return true;
        }
        if (col == 0 || col == cols - 1) {
            return true;
        }
        return false;
    }

    private void removeDeadEnd(CellPosition pos) {
        ArrayList <CellPosition> neighWalls = new ArrayList<>();
        int wallNum = 0;
        int bordNum = 0;
        for (int i = 0; i < directions.size(); i++) {
            Direction dir = directions.get(i);
            CellPosition neighPos = peekPos(pos, dir, 1);
            Tile newTile = getValue(neighPos);
            if (newTile.isWall()) {
                if (!isBorder(neighPos)) {
                    neighWalls.add(neighPos);
                    wallNum++;
                }
                else {
                    bordNum++;
                }
            }
        }
        if (wallNum + bordNum == 3) {
            Random rand = new Random();
            int index = rand.nextInt(wallNum);
            CellPosition wallPos = neighWalls.get(index);
            setValue(wallPos, new Tile(Sprite.FRUIT));
        }
    }

    private void finalizeMaze() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                CellPosition pos = new CellPosition(i, j);
                Tile tile = getValue(pos);
                if (tile.isWall()) {
                    updateWall(pos);
                }
            }
        }
    }

    private void updateWall(CellPosition pos) {
        List <Direction> directions = Arrays.asList(Direction.values());
        int [] connections = new int[] {0, 0, 0, 0};
        for (int i = 0; i < directions.size(); i++) {
            Direction dir = directions.get(i);
            CellPosition newPos = peekPos(pos, dir, 1);
            if (!isLegalTile(newPos)) {
                continue;
            }
            Tile newTile = getValue(newPos);
            if (newTile.isWall()) {
                connections[i] = 1;
            }
        }
        Tile tile = new Tile(Sprite.WALL, connections);
        setValue(pos, tile);
    }

    public void setFruits() {
        for (GridCell <Tile> cell : cells) {
            Tile tile = cell.getValue();
            if (tile.getSprite() == Sprite.FRUIT) {
                fruits++;
            }
        }
    }

    public void openCenter() {
        int midRow = (rows - 1) / 2;
        int midCol = (cols - 1) / 2;
        CellPosition tilePos = new CellPosition(midRow - 1, midCol);
        Tile tile = new Tile(Sprite.OPEN);
        setValue(tilePos, tile);
    }
}
