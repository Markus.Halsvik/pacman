package pacman.model.units;

import grid.CellPosition;
import pacman.view.tiles.Sprite;

public class Unit implements IUnit  {
    UnitType type;
    CellPosition pos;
    int health;
    Sprite sprite;
    
    public Unit(UnitType type, CellPosition pos, int health, Sprite sprite) {
        this.type = type;
        this.pos = pos;
        this.health = health;
        this.sprite = sprite;
    }

    public UnitType getType() {
        return type;
    }

    public CellPosition getPos() {
        return pos;
    }

    public void setPos(CellPosition pos) {
        this.pos = pos;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    public Sprite getSprite() {
        return sprite;
    }
    
    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
