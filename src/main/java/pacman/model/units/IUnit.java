package pacman.model.units;

import grid.CellPosition;
import pacman.view.tiles.Sprite;

public interface IUnit{
    public UnitType getType();

    public CellPosition getPos();

    public void setPos(CellPosition pos);

    public int getHealth();

    public void setHealth(int health);
    
    public Sprite getSprite();
    
    public void setSprite(Sprite sprite);

}
